<?php

    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');
    
    $sheep = new Animal("Shaun");
    echo "Name: ". $sheep->name . "<br>";
    echo "Legs: ". $sheep->legs . "<br>";
    echo "Cold Blooded: ". $sheep->cold_blooded . "<br><br>";

    $name2 = new Frog("Kodok");
    echo "Name: ". $name2->name . "<br>";
    echo "Legs: ". $name2->legs . "<br>";
    echo "Cold Blooded: ". $name2->cold_blooded . "<br>";
    $name2->jump();
    
    $nama1 = new Ape("Kera Sakti");
    echo "Name: ". $nama1->name . "<br>";
    echo "Legs: ". $nama1->legs . "<br>";
    echo "Cold Blooded: ". $nama1->cold_blooded . "<br>";
    $nama1->yell();

?>